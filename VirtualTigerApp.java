import java.util.Scanner;
public class VirtualTigerApp{
	public static void main (String[] args){
		Tiger[] ambushOfTigers = new Tiger[4];
		Scanner reader = new Scanner(System.in);
		
		for(int i = 0; i < ambushOfTigers.length; i++){
			ambushOfTigers[i] = new Tiger();
			System.out.println("What does your tiger eat?");
			ambushOfTigers[i].prey = reader.nextLine();
			System.out.println("How fast does it run?");
			ambushOfTigers[i].speed = Integer.parseInt(reader.nextLine());
			System.out.println("Which country is your tiger from?");
			ambushOfTigers[i].country = reader.nextLine();
		}
		
		System.out.println(ambushOfTigers[3].prey);
		System.out.println(ambushOfTigers[3].speed);
		System.out.println(ambushOfTigers[3].country);

		ambushOfTigers[0].saySpeed();
		ambushOfTigers[0].huntPrey();
	}
}