public class Tiger{
	public String prey;
	public int speed;
	public String country;
	
	public void saySpeed(){
		if(this.speed <= 65 && this.speed > 49){
			System.out.println("Wow! Your tiger is very fast at: " + this.speed );
		}
		if(this.speed <= 49 && this.speed > 29 ){
			System.out.println("Your tiger runs at the average speed that tigers run at: " + this.speed );
		}
		
		else if(this.speed < 29){
			System.out.println("Your tiger runs pretty slowly for a tiger at: " + this.speed);
		}
	}
	public void huntPrey(){
		if(this.speed < 49){
			System.out.println("Your tiger is not trying too hard to hunt " + this.prey + " tell it to try harder");
		}
		else{
			System.out.println("Wow your tiger is running very fast to hunt " + this.prey + " you should go congratulate it");
		}
	}
	
}